﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoProject
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string Con = ConfigurationManager.ConnectionStrings["DemoP"].ConnectionString;
            using (SqlConnection co = new SqlConnection(Con))
            {
                co.Open();
                SqlCommand cmd = new SqlCommand("SELECT [WareHouseId] ,[WareHouseName] FROM [dbo].[WareHouse]", co);
                DropDownList1.DataSource = cmd.ExecuteReader();
                DropDownList1.DataTextField = "WareHouseName";
                DropDownList1.DataValueField = "WareHouseId";
                DropDownList1.DataBind();
                co.Close();

                co.Open();
                SqlCommand itemCmd = new SqlCommand("SELECT [ItemId],[ItemName],[CategoryId] FROM [dbo].[Item]", co);
                DropDownList2.DataSource = itemCmd.ExecuteReader();
                DropDownList2.DataTextField = "[ItemName]";
                DropDownList2.DataValueField = "[ItemId]";
                DropDownList2.DataBind();
                co.Close();


            }
        }
    }
}