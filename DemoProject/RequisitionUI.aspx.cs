﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Web.Configuration;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using DemoProject.Models;

namespace DemoProject
{
    public partial class RequisitionUI : System.Web.UI.Page
    {
      
        private string connectionString = WebConfigurationManager.ConnectionStrings["DemoProject"].ConnectionString;
        private ArrayList GetItemData()
        {

            ArrayList arr = new ArrayList();

            SqlConnection connection = new SqlConnection(connectionString);
            string query = "SELECT * FROM Item";
            SqlCommand Command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader Reader = Command.ExecuteReader();
            


            while (Reader.Read())
            {
                Item item = new Item();
                int ItemId = Convert.ToInt32(Reader["ItemId"]);
                string ItemName = Reader["ItemName"].ToString();
                item.CategoryId= Convert.ToInt32(Reader["CategoryId"]);

                //arr.Add(item);
                arr.Add(new ListItem(ItemName, ItemId.ToString()));
            }

            Reader.Close();
            connection.Close();

            return arr;
        }

        private void FillDropDownList(DropDownList ddl)
        {
            ArrayList arr = GetItemData();

            foreach (ListItem item in arr)
            {
                ddl.Items.Add(item);
            }
        }

        private void SetInitialRow()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

           
            dt.Columns.Add(new DataColumn("Column1", typeof(string)));//for TextBox value   
            dt.Columns.Add(new DataColumn("Column2", typeof(string)));//for TextBox value   
            dt.Columns.Add(new DataColumn("Column3", typeof(string)));//for DropDownList selected item   
            dt.Columns.Add(new DataColumn("Column4", typeof(string)));//for DropDownList selected item   

            dr = dt.NewRow();
          
            dr["Column2"] = string.Empty;
            dr["Column3"] = string.Empty;
            dr["Column4"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState for future reference   
            ViewState["CurrentTable"] = dt;

            //Bind the Gridview   
            Gridview1.DataSource = dt;
            Gridview1.DataBind();

            //After binding the gridview, we can then extract and fill the DropDownList with Data   
            DropDownList ddl1 = (DropDownList)Gridview1.Rows[0].Cells[1].FindControl("DropDownList1");
           
            FillDropDownList(ddl1);
          
        }

        private void AddNewRowToGrid()
        {

            if (ViewState["CurrentTable"] != null)
            {

                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    drCurrentRow = dtCurrentTable.NewRow();
                   // drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                    //add new row to DataTable   
                    dtCurrentTable.Rows.Add(drCurrentRow);

                    //Store the current data to ViewState for future reference   
                    ViewState["CurrentTable"] = dtCurrentTable;


                    for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                    {

                        //extract the TextBox values   
                        DropDownList ddl1 = (DropDownList)Gridview1.Rows[i].Cells[1].FindControl("DropDownList1");
                        dtCurrentTable.Rows[i]["Column1"] = ddl1.SelectedItem.Text;

                        TextBox box1 = (TextBox)Gridview1.Rows[i].Cells[2].FindControl("TextBox1");
                        TextBox box2 = (TextBox)Gridview1.Rows[i].Cells[3].FindControl("TextBox2");
                        TextBox box3 = (TextBox)Gridview1.Rows[i].Cells[4].FindControl("TextBox3");
                        dtCurrentTable.Rows[i]["Column2"] = box1.Text;
                        dtCurrentTable.Rows[i]["Column3"] = box2.Text;
                        dtCurrentTable.Rows[i]["Column4"] = box3.Text;
                       
                    }

                    //Rebind the Grid with the current data to reflect changes   
                    Gridview1.DataSource = dtCurrentTable;
                    Gridview1.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");

            }
            //Set Previous Data on Postbacks   
            SetPreviousData();
        }

        private void SetPreviousData()
        {

            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        DropDownList ddl1 = (DropDownList)Gridview1.Rows[rowIndex].Cells[1].FindControl("DropDownList1");
                        FillDropDownList(ddl1);
                        TextBox box1 = (TextBox)Gridview1.Rows[i].Cells[1].FindControl("TextBox1");
                        TextBox box2 = (TextBox)Gridview1.Rows[i].Cells[2].FindControl("TextBox2");
                        TextBox box3 = (TextBox)Gridview1.Rows[i].Cells[2].FindControl("TextBox3");

                        if (i < dt.Rows.Count - 1)
                        {
                            //Set the Previous Selected Items on Each DropDownList  on Postbacks   
                            ddl1.ClearSelection();
                            
                            ddl1.Items.FindByText(dt.Rows[i]["Column1"].ToString()).Selected = true;
                            ////Assign the value from DataTable to the TextBox   
                            box1.Text = dt.Rows[i]["Column2"].ToString();
                            box2.Text = dt.Rows[i]["Column3"].ToString();
                            box3.Text = dt.Rows[i]["Column4"].ToString();
                        }

                        rowIndex++;
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetInitialRow();
                GetAllWareHouseDataBindWithDropDown();
             
            }
        }

        private void GetAllWareHouseDataBindWithDropDown()
        {
            warehouseDropDownList.DataSource = GetAllWarehouse();
            warehouseDropDownList.DataTextField = "WareHouseName";
            warehouseDropDownList.DataValueField = "WareHouseId";
            warehouseDropDownList.DataBind();
            warehouseDropDownList.Items.Insert(0, "--Select Category--");
        }

 

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        protected void Gridview1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                LinkButton lb = (LinkButton)e.Row.FindControl("LinkButton1");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (gvRow.RowIndex < dt.Rows.Count - 1)
                    {
                        //Remove the Selected Row data and reset row number  
                        dt.Rows.Remove(dt.Rows[rowID]);
                        ResetRowID(dt);
                    }
                }

                //Store the current data in ViewState for future reference  
                ViewState["CurrentTable"] = dt;

                //Re bind the GridView for the updated data  
                Gridview1.DataSource = dt;
                Gridview1.DataBind();
            }

            //Set Previous Data on Postbacks  
            SetPreviousData();
        }

        private void ResetRowID(DataTable dt)
        {
            int rowNumber = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row[0] = rowNumber;
                    rowNumber++;
                }
            }
        }

        private void InsertRecords(StringCollection sc, int Rid)
        {
            StringBuilder sb = new StringBuilder(string.Empty);
            string[] splitItems = null;
            const string sqlStatement = "INSERT INTO Details (ItemId,Brand,Model,Color,RequisitionId) VALUES";
            foreach (var item in sc)
            {
                if (item.Contains(","))
                {
                    splitItems = item.Split(",".ToCharArray());
                    sb.AppendFormat("{0}('{1}','{2}','{3}','{4}','{5}'); ", sqlStatement, splitItems[0], splitItems[1], splitItems[2], splitItems[3], Rid);
                }
            }

            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand(sb.ToString(), connection);
            cmd.CommandType = CommandType.Text;
            connection.Open();
            cmd.ExecuteNonQuery();
            connection.Close();

            lblMessage.Text = "Records successfully saved!";
        }



        protected void BtnSave_Click(object sender, EventArgs e)
        {

           // Requisition req = new Requisition();
            DateTime RequisitionDate = Convert.ToDateTime(ReqDate.Value);
            string warehouseId = (warehouseDropDownList.Text);

            SqlConnection connection = new SqlConnection(connectionString);
           
            string query = @"INSERT INTO Requisition(Date,WareHouseId) VALUES( GETDATE()  , '"+warehouseId+"' )";
            SqlCommand command = new SqlCommand(query, connection);
            //command.Parameters.AddWithValue("@Date ", RequisitionDate);
            //command.Parameters.AddWithValue("@WareHouseId ", warehouseId);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();

            string query1 = "SELECT MAX(RequisitionId) FROM Requisition ";
            SqlCommand command1 = new SqlCommand(query1, connection);
            connection.Open();
            int rID = Convert.ToInt32(command1.ExecuteScalar());
            int rowIndex = 0;
            StringCollection sc = new StringCollection();
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values  
                        DropDownList ddl1 = (DropDownList)Gridview1.Rows[rowIndex].Cells[1].FindControl("DropDownList1");
                        TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("TextBox1");
                        TextBox box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("TextBox2");
                        TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("TextBox3");
                        
                        //get the values from TextBox and DropDownList  
                        //then add it to the collections with a comma "," as the delimited values  
                        sc.Add(string.Format("{0},{1},{2},{3}", ddl1.SelectedValue, box1.Text, box2.Text, box3.Text));
                        rowIndex++;
                    }
                    //Call the method for executing inserts  
                    InsertRecords(sc,rID);
                }
            }
        }

        private List<WareHouse> GetAllWarehouse()
        {

            SqlConnection connection = new SqlConnection(connectionString);
            string query = "SELECT * FROM WareHouse";

            SqlCommand Command = new SqlCommand(query, connection);
            connection.Open();

            SqlDataReader Reader = Command.ExecuteReader();
            List<WareHouse> warehouse = new List<WareHouse>();

            while (Reader.Read())
            {
                WareHouse wareHouse = new WareHouse();
                wareHouse.WareHouseId = Convert.ToInt32(Reader["WareHouseId"]);
                wareHouse.WareHouseName = Reader["WareHouseName"].ToString();

                warehouse.Add(wareHouse);
            }

            Reader.Close();
            connection.Close();

            return warehouse;
        }
    }
}
