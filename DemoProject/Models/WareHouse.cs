﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoProject.Models
{
    public class WareHouse
    {
        public int WareHouseId { get; set; }
        public string WareHouseName { get; set; }

        public WareHouse()
        {

        }
    }
}