﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoProject.Models
{
    public class ReqDetails
    {
        public int ItemId { set; get; }
        public string Brand { set; get; }
        public string Model { set; get; }
        public string Color { set; get; }
    }
}
