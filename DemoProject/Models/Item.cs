﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoProject.Models
{
    public class Item
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }

        public int CategoryId { get; set; }
        public Item()
        {

        }
    }
}