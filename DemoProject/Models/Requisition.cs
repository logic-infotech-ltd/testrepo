﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoProject.Models
{
    public class Requisition
    {
        public int RequisitionId { get; set; }
        public DateTime Date { get; set; }
        public int WareHouseId { get; set; }

        public Requisition()
        {

        }
    }
}