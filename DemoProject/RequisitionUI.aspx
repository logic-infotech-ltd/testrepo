﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RequisitionUI.aspx.cs" Inherits="DemoProject.RequisitionUI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .centered {
  display: flex;
  align-items: center;
  justify-content: center;
}
    </style>


    <div class="jumbotron">
        <div class="row">
            <div class="" style="background-color: #3498DB; color: white; text-align: center; padding:5px; margin:5px">
                <h3>Requisition Information</h3>
                 
            </div><br />
            <p><asp:Label ID="lblMessage" runat="server" CssClass="bg-primary" Text=""></asp:Label></p>
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="ReqDate">Req. Date: </label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input type="date" id="ReqDate" runat="server" class="form-control" placeholder="Enter Requisition Date">
                    </div>
                </div>

                <div class="form-group">
                    <label for="warehouseDropDownList">Warehouse :</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                        <asp:DropDownList ID="warehouseDropDownList" Width="300px" runat="server" CssClass="add-company-form-dropdown" Height="37px">
                            <asp:ListItem Text="--Select WareHouse--" Value="" />
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>

        <div class="row"></div>

        <h3>Requisition Details:</h3>

        <asp:GridView ID="Gridview1" runat="server" ShowFooter="true"
            AutoGenerateColumns="false" CssClass="table table-striped table-hover"
            OnRowCreated="Gridview1_RowCreated">
            <Columns>
                <asp:TemplateField HeaderText="Item">
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server"
                            AppendDataBoundItems="true" Width="100%" Height="100%">
                            <asp:ListItem Value="-1">--Select Item--</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Brand">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Width="100%" Height="100%"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Model">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Width="100%" Height="100%"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Color">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox3" runat="server" Width="100%" Height="100%"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server"
                            OnClick="LinkButton1_Click">Remove</asp:LinkButton>
                    </ItemTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                    <FooterTemplate>
                        <asp:Button ID="ButtonAdd" runat="server"
                            Text="Add New Row"
                            OnClick="ButtonAdd_Click" />
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <br />

        <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="BtnSave_Click" />


    </div>

</asp:Content>
