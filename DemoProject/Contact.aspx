﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="DemoProject.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />
    <div style="width:900px; margin:auto;">
        <div class="panel-heading bg-primary text-center">
            <strong>Requisition Information</strong>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" Text="Requisition Date: "></asp:Label>
                    <input type="date" class="form-control" id="txtDate" />
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" Text="Ware House: "></asp:Label>
                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" Text="Item Name: "></asp:Label>
                    <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" Text="Brand: "></asp:Label>
                    <asp:TextBox ID="TxtBrand" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" Text="Model: "></asp:Label>
                    <asp:TextBox ID="txtModel" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" Text="Color: "></asp:Label>
                    <asp:TextBox ID="txtColor" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3">
                <asp:Button ID="Button1" runat="server" Text="Button" CssClass="btn btn-primary" />
                <asp:Button ID="Button2" runat="server" Text="Button" CssClass="btn btn-primary" />
            </div>
        </div>

    </div>
</asp:Content>
