﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Requisition.aspx.cs" Inherits="DemoProject.Requisition" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="col-md-10 col-md-offset-2">
        <div class="row" style="margin-top:10px;">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#d73b3b; color:white">
                    <strong>Insert Requisition Details information</strong>
                </div>
                <div class="panel-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="dateField">Date: </label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <asp:TextBox ID="txtDate" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="WareHouseField">WareHouse: </label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                <asp:TextBox ID="txtWareHouse" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-heading" style="background-color:#d73b3b; color:white">
                    <strong>Requisition Details Part</strong>
                </div>
                <div class="panel-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="ItemField">Item Name: </label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                <asp:DropDownList ID="ItemDropDown" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="brandField">Brand: </label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                <asp:TextBox ID="TxtBrand" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-3">
                        <div class="form-group">
                            <label for="modelField">Model: </label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                <asp:TextBox ID="txtModel" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-3">
                        <div class="form-group">
                            <label for="colorField">Color: </label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                <asp:TextBox ID="txtColor" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="RequisitionIdField">Requisition ID: </label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                <asp:TextBox ID="txtRequisitionId" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" CssClass="btn btn-success" Font-Bold="true" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-danger" Font-Bold="true" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
