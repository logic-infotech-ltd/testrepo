﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DemoProject
{
    public partial class Requisition : System.Web.UI.Page
    {
        string Con = ConfigurationManager.ConnectionStrings["DemoP"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using(SqlConnection co = new SqlConnection(Con))
            {
                co.Open();
                SqlCommand cmd = new SqlCommand("insert into [Requisition]  values('" + txtDate.Text + "'," + txtWareHouse.Text + ")", co);
                cmd.ExecuteNonQuery();

                string query = "SELECT MAX([RequisitionId]) AS LastID FROM [Requisition]";
                SqlCommand SDA = new SqlCommand(query, co);
                int myID = Convert.ToInt32(SDA.ExecuteScalar());



                SqlCommand cmd2 = new SqlCommand("INSERT INTO [dbo].[Details] VALUES(" + ItemDropDown.Text + ",'" + TxtBrand.Text + "','"+ txtModel.Text + "','"+ txtColor.Text + "',"+ myID + ")", co);
                int i = cmd2.ExecuteNonQuery();
                if (i > 0)
                {
                    Console.Write("<script>alert('Data has been submitted successfully')</script>");
                }
                co.Close();
                
            }
        }
    }
}